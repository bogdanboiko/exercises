import Exercises.BinaryTree;
import Exercises.WeightNumbers;

import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        BinaryTree<String, Integer> tree = new BinaryTree<>((integer, t1) -> {
            if (integer > t1) {
                return 1;
            } else if (integer < t1) {
                return -1;
            } else {
                return 0;
            }
        });

        tree.add("Hello world", 10);
        tree.add("Hi", 20);
        tree.add("Bye", -2);
        System.out.println(tree.get(10) + " " + tree.get(20) + " " + tree.get(-2));
        System.out.println(tree.size());
        tree.remove(-2);
        System.out.println(tree.size());
    }
}
